```bash
dotnet new blazorserver -o BlazorServerSignalRApp
cd BlazorServerSignalRApp

dotnet add package Microsoft.AspNetCore.SignalR.Client

dotnet run
```
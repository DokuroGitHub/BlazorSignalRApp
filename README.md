```bash
dotnet new blazorserver -o BlazorServerSignalRApp
cd BlazorServerSignalRApp

dotnet new blazorwasm -ho -o BlazorWebAssemblySignalRApp
cd BlazorWebAssemblySignalRApp

dotnet add package Microsoft.AspNetCore.SignalR.Client

dotnet add Client package Microsoft.AspNetCore.SignalR.Client
dotnet add Share package Microsoft.AspNetCore.SignalR

dotnet new gitignore
git init
git add .
git commit -m "c1 Init"
git push --set-upstream https://gitlab.com/DokuroGitHub/BlazorSignalRApp.git master
```
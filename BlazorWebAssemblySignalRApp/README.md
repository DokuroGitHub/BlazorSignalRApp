```bash
dotnet new blazorwasm -ho -o BlazorWebAssemblySignalRApp
cd BlazorWebAssemblySignalRApp

dotnet add Client package Microsoft.AspNetCore.SignalR.Client
dotnet add Share package Microsoft.AspNetCore.SignalR

cd Server
dotnet run
```